# README #


### What is this repository for? ###

A blob counter in Python, designed to count cells in microscope samples. Given some image, it counts the number of colored blobs within a set boundary. Various params can be set, using the blobdetection algorithm from OpenCV.

![Example](images/example_ui.jpg)

The left panel shows the original image and the blob detection parameters. The right panel includes the grayscale processed image with blobs marked by red circles, cropped to the selected area and with a total blob count.

A saved processed image will look something like this:

![Example](images/example_export.jpg)

### How do I get set up? ###

Install Python and OpenCV, run main.py