from tkinter import Tk, Label, Button, Checkbutton, Entry, IntVar, StringVar, END, W, E, filedialog, NORMAL, DISABLED
from PIL import Image, ImageTk
import cv2
import numpy as np
import ntpath


# BLOB COUNT INITIAL VALUES
# =========================

MINTHRESHOLD          = 1
MAXTHRESHOLD          = 200

FILTERBYCOLOR         = False
BLOBCOLOR             = 255 # must be 0 or 255

FILTERBYAREA          = True
MINAREA               = 100
MAXAREA               = ''

FILTERBYCIRCULARITY   = False
MINCIRCULARITY        = 0.7
MAXCIRCULARITY        = 1

FILTERBYCONVEXITY     = False
MINCONVEXITY          = 0.5
MAXCONVEXITY          = 1

FILTERBYINERTIA       = True
MININERTIARATIO       = 0.1
MAXINERTIARATIO       = 1


# UI VALUES
# =========================

MAX_IMAGE_WIDTH       = 800
MAX_IMAGE_HEIGHT      = 400

BUTTON_WIDTH          = 14


class BlobCountUI:

    def __init__(self, master):
        master.title("Blob Count")
        grid_row = 1
        self.im_original = None
        self.im_processed = None
        self.im_original_panel = None
        self.im_processed_panel = None
        self.path = None
        self.im_roi = None

        self.params = [
          { 'name': 'minThreshold', 'label': 'Min Treshold', 'var': StringVar(), 'value': MINTHRESHOLD, 'type': 'TEXT' },
          { 'name': 'maxThreshold', 'label': 'Max Treshold', 'var': StringVar(), 'value': MAXTHRESHOLD, 'type': 'TEXT' },
          { 'name': 'filterByColor', 'label': 'Filter by Colour', 'var': IntVar(), 'value': FILTERBYCOLOR, 'type': 'CHECK' },
          { 'name': 'blobColor', 'label': 'Blob Colour (0 or 255)', 'var': StringVar(), 'value': BLOBCOLOR, 'type': 'TEXT' },
          { 'name': 'filterByArea', 'label': 'Filter by Area', 'var': IntVar(), 'value': FILTERBYAREA, 'type': 'CHECK' },
          { 'name': 'minArea', 'label': 'Min Area', 'var': StringVar(), 'value': MINAREA, 'type': 'TEXT' },
          { 'name': 'maxArea', 'label': 'Max Area', 'var': StringVar(), 'value': MAXAREA, 'type': 'TEXT' },
          { 'name': 'filterByCircularity', 'label': 'Filter by Circularity', 'var': IntVar(), 'value': FILTERBYCIRCULARITY, 'type': 'CHECK' },
          { 'name': 'minCircularity', 'label': 'Min Circularity', 'var': StringVar(), 'value': MINCIRCULARITY, 'type': 'TEXT' },
          { 'name': 'maxCircularity', 'label': 'Max Circularity', 'var': StringVar(), 'value': MAXCIRCULARITY, 'type': 'TEXT' },
          { 'name': 'filterByConvexity', 'label': 'Filter by Convexity', 'var': IntVar(), 'value': FILTERBYCONVEXITY, 'type': 'CHECK' },
          { 'name': 'minConvexity', 'label': 'Min Convexity', 'var': StringVar(), 'value': MINCONVEXITY, 'type': 'TEXT' },
          { 'name': 'maxConvexity', 'label': 'Max Convexity', 'var': StringVar(), 'value': MAXCONVEXITY, 'type': 'TEXT' },
          { 'name': 'filterByInertia', 'label': 'Filter by Inertia', 'var': IntVar(), 'value': FILTERBYINERTIA, 'type': 'CHECK' },
          { 'name': 'minInertiaRatio', 'label': 'Min Inertia', 'var': StringVar(), 'value': MININERTIARATIO, 'type': 'TEXT' },
          { 'name': 'maxInertiaRatio', 'label': 'Max Inertia', 'var': StringVar(), 'value': MAXINERTIARATIO, 'type': 'TEXT' },
        ]

        self.select_image_button = Button(master, text="Select Image", command=lambda: self.select_image(), width=BUTTON_WIDTH)
        self.select_image_button.grid(row=grid_row, column=2, columnspan=1, sticky=W, padx=(40, 0))
        self.im_path_label = Label(master, text='')
        self.im_path_label.grid(row=grid_row, column=3, columnspan=1, sticky=W)

        self.apply_button = Button(master, text="Count Blobs", command=lambda: self.detect_blobs(), state=DISABLED, width=BUTTON_WIDTH)
        self.apply_button.grid(row=grid_row+1, column=2, columnspan=1, sticky=W, padx=(40, 0))
        self.blob_count_label = Label(master, text='')
        self.blob_count_label.grid(row=grid_row+1, column=3, columnspan=1, sticky=W)

        self.select_boundary_button = Button(master, text="Select Area", command=lambda: self.select_image_boundary(), state=DISABLED, width=BUTTON_WIDTH)
        self.select_boundary_button.grid(row=grid_row+2, column=2, columnspan=1, sticky=W, padx=(40, 0))

        self.save_button = Button(master, text="Save Image", command=lambda: self.save_processed_image(), state=DISABLED, width=BUTTON_WIDTH)
        self.save_button.grid(row=grid_row+3, column=2, columnspan=1, sticky=W, padx=(40, 0))

        for param in self.params:
          param['var'].set(param['value'])
          if param['type'] == 'TEXT':
            self.add_text_field(master, param['label'], param['var'], grid_row)
          elif param['type'] == 'CHECK':
            self.add_checkbox(master, param['label'], param['var'], grid_row)
          grid_row += 1

    def detect_blobs(self):
        if self.path is not None and len(self.path) > 0:
          im_processed = cv2.imread(self.path, cv2.IMREAD_GRAYSCALE)

          # crop image
          if self.im_roi is not None:
            r = self.im_roi
            im_processed = im_processed[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]

          # apply params
          params = cv2.SimpleBlobDetector_Params()
          for param in self.params:
            val = param['var'].get()
            if val == '':
              continue
            val = float(val) if isinstance(val, str) else val
            setattr(params, param['name'], val)

          # detect blobs
          detector = cv2.SimpleBlobDetector_create(params)
          self.keypoints = detector.detect(im_processed)
          self.blob_count_label['text'] = '{} blobs'.format(len(self.keypoints))
          self.im_processed = cv2.drawKeypoints(im_processed, self.keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
          self.show_images()

    def add_text_field(self, frame, label, var, grid_row):
        label = Label(frame, text=label)
        entry = Entry(frame, textvariable=var)
        label.grid(row=grid_row, column=0, columnspan=1, sticky=W)
        entry.grid(row=grid_row, column=1, columnspan=1, sticky=E)

    def add_checkbox(self, frame, label, var, grid_row):
        check = Checkbutton(frame, text=label, variable=var)
        check.grid(row=grid_row, column=1, columnspan=1, sticky=E)

    def select_image(self):
        self.path = filedialog.askopenfilename(title="Select an image")
        if len(self.path) > 0:
          self.im_original = cv2.imread(self.path)
          self.im_roi = None # reset boundary
          self.im_file = ntpath.basename(self.path)
          self.im_path_label['text'] = self.im_file
          self.detect_blobs()
          self.show_images()
          self.apply_button['state'] = NORMAL
          self.save_button['state'] = NORMAL
          self.select_boundary_button['state'] = NORMAL

    def select_image_boundary(self):
        if self.path is not None and len(self.path) > 0:
          # TODO there must be a better way to do this..
          im_original = Image.fromarray(self.im_original)
          scale_params = self.image_scale(im_original, MAX_IMAGE_WIDTH*1.5, MAX_IMAGE_HEIGHT*1.5)
          im_original = im_original.resize(scale_params['scale'], Image.ANTIALIAS)
          im_original = np.asarray(im_original)
          roi = cv2.selectROI(im_original, False)
          self.im_roi = tuple(map(lambda x: int(x/scale_params['ratio']), roi))
          cv2.waitKey(0)
          cv2.destroyAllWindows()
          self.detect_blobs()

    def image_scale(self, image, max_width, max_height):
        width, height = image.size
        ratio = min(max_width/width, max_height/height)
        scale = (int(ratio*width), int(ratio*height))
        return { 'ratio': ratio, 'scale': scale }

    def show_images(self):
        im_original = Image.fromarray(cv2.cvtColor(self.im_original, cv2.COLOR_BGR2RGB))
        im_processed = Image.fromarray(cv2.cvtColor(self.im_processed, cv2.COLOR_BGR2RGB))

        # resize image to fit screen, the thumbnail method doesnt seem to work?
        self.scale_params = self.image_scale(im_original, MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT)
        im_original = ImageTk.PhotoImage(im_original.resize(self.scale_params['scale'], Image.ANTIALIAS))
        im_processed = ImageTk.PhotoImage(im_processed.resize(self.scale_params['scale'], Image.ANTIALIAS))

        # initialize the image panels
        if self.im_original_panel is None or self.im_processed_panel is None:
          self.im_original_panel = Label(image=im_original)
          self.im_original_panel.grid(row=0, column=0, columnspan=2, sticky=W)
          self.im_processed_panel = Label(image=im_processed)
          self.im_processed_panel.grid(row=0, column=2, columnspan=2, sticky=E)
        else:
          self.im_original_panel.configure(image=im_original)
          self.im_processed_panel.configure(image=im_processed)

        self.im_original_panel.image = im_original
        self.im_processed_panel.image = im_processed

    def save_processed_image(self):
        if self.path is not None and len(self.path) > 0:
          count = 'blobcount {}'.format(len(self.keypoints))
          text_scale = self.scale_params['ratio']
          cv2.putText(self.im_processed, count, (20,50), cv2.FONT_HERSHEY_SIMPLEX, text_scale, 255, 1)
          im_processed = Image.fromarray(cv2.cvtColor(self.im_processed, cv2.COLOR_BGR2RGB))
          im_processed.save('blobs_{}'.format(self.im_file))


root = Tk()
root.resizable(0, 0)
ui = BlobCountUI(root)
def submit_on_enter(event):
  ui.detect_blobs()
root.bind('<Return>', submit_on_enter)
root.mainloop()
